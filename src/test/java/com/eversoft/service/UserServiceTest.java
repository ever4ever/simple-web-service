package com.eversoft.service;

import com.eversoft.dto.UserDto;
import com.eversoft.model.Profile;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.RoleRepository;
import com.eversoft.repository.UserRepository;
import com.eversoft.service.profile.ProfileService;
import com.eversoft.service.role.RoleService;
import com.eversoft.service.roletype.RoleTypeService;
import com.eversoft.service.user.UserService;
import com.eversoft.service.user.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    private UserService userService;
    private RoleTypeService roleTypeService;
    private RoleService roleService;
    @Mock
    private ProfileService profileService;
    @Mock
    private ProfileRepository profileRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;


    @Before
    public void setUp() {
        PasswordEncoder passwordEncoder=mock(PasswordEncoder.class);
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        profileService= mock(ProfileService.class);
        userService = new UserServiceImpl(userRepository, modelMapper , roleService,roleTypeService,profileService);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        //roleService = new RoleServceImp(roleRepository, modelMapper);
         roleService = mock(RoleService.class);
        when(SecurityContextHolder.getContext().getAuthentication().getName())
                .thenReturn("TEST USER");
    }

    @Test
    public void testFindUserByEmail() {
        final String id = "testid";
        final String email = "sylwia@go.pl";

        when(userRepository.findByEmail(email))
                .thenReturn(Optional.of(User.builder()
                        .id(id)
                        .email(email)
                        .build()));
        UserDto userDto = userService.getUserByEmail(email);

        Assert.assertEquals(email, userDto.getEmail());
        Assert.assertEquals(id, userDto.getId());
    }




    @Test
    public void testGetUserById() {
        String userId = "idjeden";
        User user = User.builder()
                .id("idjeden")
                .email("test@go.pl")
                .profile(Profile.builder().fbId("OLDFB").linkedinId("OLDLD").build())
                .build();

        when(userRepository.findByEmail(userId)).thenReturn(Optional.ofNullable(user));
        UserDto userGet = userService.getUserByEmail(userId);

        Assert.assertEquals("test@go.pl", userGet.getEmail());
        Assert.assertEquals("idjeden", userGet.getId());
    }



    @Test
    public void testEditUserByUserId() {
        UserDto userDto = new UserDto();
        userDto.setId("idjeden");
        userDto.setName("noweImie");
        userDto.setSurname("noweNazwisko");

        User user = User.builder()
                .id("idjeden")
                .deleted(false)
                .name("stareImie")
                .surname("stareNazwisko")
                .email("test@go.pl")
                .profile(Profile.builder().id("profileid").fbId("OLDFB").linkedinId("OLDLD").build())
                .build();

        when(userRepository.findById(userDto.getId())).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(user)).then(AdditionalAnswers.returnsFirstArg());

        UserDto userChanged = this.userService.editUserByUserId(userDto);
        Assert.assertEquals( userDto.getName(), userChanged.getName());
        Assert.assertEquals( userDto.getSurname(), userChanged.getSurname());
    }



//    @Test
//    public void testDeleteUserByUserId() {
//        String userId= "userId";
//
//        User user = User.builder()
//                .id("idjeden")
//                .deleted(false)
//                .name("stareImie")
//                .surname("stareNazwisko")
//                .email("test@go.pl")
//                .profile(Profile.builder().id("profileid").fbId("OLDFB").linkedinId("OLDLD").build())
//                .build();
//
//
//        Collection<OAuth2AccessToken> tokens = Arrays.asList();
//
//        when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(user));
//        when(this.profileService.save(user.getProfile())).then(AdditionalAnswers.returnsFirstArg());
//        when(userRepository.save(any(User.class))).then(AdditionalAnswers.returnsFirstArg());
//
//        UserDto userDeleted = this.userService.deleteUserByUserId(userId);
//        Assert.assertEquals(true, userDeleted.getDeleted());
//    }



//    @Test
//    public void testRegisterUser() {
//        UserRegisterDto userRegisterDto = new UserRegisterDto();
//        userRegisterDto.setEmail("test@email.com");
//        userRegisterDto.setPassword("pass");
//        userRegisterDto.setPasswordReEnter("pass");
//        userRegisterDto.setName("test name");
//        userRegisterDto.setSurname("test surnname");
//
//        User user = new User();
//        when(userRepository.save(any(User.class))).then(AdditionalAnswers.returnsFirstArg());
//        when(this.roleService.saveRole(any(ResponseSimpleModel.class))).then(AdditionalAnswers.returnsFirstArg());
//        when(this.passwordEncoder.encode(any(String.class))).thenReturn("CODED");
//
//
//         UserDto userDto = this.userService.registerUser(userRegisterDto);
//
//        Assert.assertEquals(userRegisterDto.getSurname(), userDto.getSurname());
//    }
    @Test
    public void testFindUsers() {

        Assert.assertEquals("2", "2");
    }
}
