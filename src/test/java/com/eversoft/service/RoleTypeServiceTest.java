package com.eversoft.service;

import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;
import com.eversoft.repository.RoleTypeRepository;
import com.eversoft.service.roletype.RoleTypeService;
import com.eversoft.service.roletype.RoleTypeServiceImp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class RoleTypeServiceTest {

    @Mock
    private RoleTypeRepository roleTypeRepository;
    private RoleTypeService roleTypeService;

    @Before
    public void setUp() {
        roleTypeService = new RoleTypeServiceImp(roleTypeRepository);
    }

    @Test
    public void findByNameTest() {

        RoleTypeEnum name = RoleTypeEnum.USER;

        when(roleTypeRepository.findByName(name)).thenReturn(java.util.Optional.ofNullable(RoleType.builder()
                .name(name)
                .build()));
        RoleType roleTypeResponse = this.roleTypeService.getRoleByName(name);
        Assert.assertEquals(name, roleTypeResponse.getName());
    }
}
