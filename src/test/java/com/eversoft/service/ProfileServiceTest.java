package com.eversoft.service;

import com.eversoft.dto.ProfileDto;
import com.eversoft.model.Profile;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.UserRepository;
import com.eversoft.service.profile.ProfileService;
import com.eversoft.service.profile.ProfileServiceImp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ProfileServiceTest {

    @Mock
    private ProfileRepository profileRepository;
    @Mock
    private UserRepository userRepository;
    private ProfileService profileService;

    @Before
    public void setUp() {
        ModelMapper modelMapper = new ModelMapper();
        profileService = new ProfileServiceImp(profileRepository, modelMapper, userRepository);
    }

    @Test
    public void saveProfileTest() {
        String id = "id";
        String profileFbId = "fbid";
        String profileLinkedinId = "ldnid";
        Profile profile = Profile.builder()
                .fbId(profileFbId)
                .linkedinId(profileLinkedinId)
                .build();

        ProfileDto profileDto = new ProfileDto();
        profileDto.setLinkedinId("ldnid");
        profileDto.setFbId("fbid");
        when(profileRepository.save(ArgumentMatchers.any(Profile.class))).thenReturn(Profile.builder()
                .id(id)
                .fbId(profileFbId)
                .linkedinId(profileLinkedinId)
                .build());


        ProfileDto profile1Response = this.profileService.save(profileDto);

        Assert.assertEquals(profileFbId, profile1Response.getFbId());
        Assert.assertEquals(profileLinkedinId, profile1Response.getLinkedinId());
    }

    //    @Test
//    public void testDeleteUserProfileByUserId() {
//        String userId = "idjeden";
//        User user = User.builder()
//                .id("idjeden")
//                .deleted(false)
//                .email("test@go.pl")
//                .profile(Profile.builder().id("profileid").fbId("OLDFB").linkedinId("OLDLD").build())
//                .build();
//        ProfileDto profile = new ProfileDto();
//        profile.setLinkedinId("OLDLD");
//        profile.setFbId("OLDFB");
//
//        when(userRepository.findByEmail(userId)).thenReturn(Optional.ofNullable(user));
//        when(profileService.save(any(ProfileDto.class))).then(AdditionalAnswers.returnsFirstArg());
//        when(userRepository.save(user)).then(AdditionalAnswers.returnsFirstArg());
//        when(profileRepository.save(any(Profile.class))).then(AdditionalAnswers.returnsFirstArg());
//        UserDto userChanged = profileService.deleteUserProfileByUserId(userId);
//
//        Assert.assertEquals(false, userChanged.getDeleted());
//    }
    @Test
    public void testEditUserProfileByUserId() {
        String userId = "idjede";
        ProfileDto profileDtoNew = new ProfileDto();
        profileDtoNew.setFbId("FB");
        profileDtoNew.setLinkedinId("LD");
        User user = User.builder()
                .id("id")
                .email("test@go.pl")
                .profile(Profile.builder().fbId("OLDFB").linkedinId("OLDLD").build())
                .build();

        when(userRepository.findByEmail(userId)).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(any(User.class))).then(AdditionalAnswers.returnsFirstArg());

        ProfileDto profileDto = profileService.editUserProfileByUserId(userId, profileDtoNew);
        Assert.assertEquals(profileDto.getLinkedinId(), "LD");
        Assert.assertEquals(profileDto.getFbId(), "FB");
    }

    @Test
    public void testGetUserProfileByUserId() {
        String userId = "idjede";
        User user = User.builder()
                .id("id")
                .email("test@go.pl")
                .profile(Profile.builder().fbId("OLDFB").linkedinId("OLDLD").build())
                .build();

        when(userRepository.findByEmail(userId)).thenReturn(Optional.ofNullable(user));


        ProfileDto profileDto = profileService.getUserProfileByUserId(userId);
        Assert.assertEquals(profileDto.getLinkedinId(), "OLDLD");
        Assert.assertEquals(profileDto.getFbId(), "OLDFB");
    }

    @Test
    public void testEditUserProfile() {
        String email = "test@go.pl";
        ProfileDto profileDtoNew = new ProfileDto();
        profileDtoNew.setFbId("FB");
        profileDtoNew.setLinkedinId("LD");

        User user = User.builder()
                .id("id")
                .email("test@go.pl")
                .profile(Profile.builder().fbId("OLDFB").linkedinId("OLDLD").build())
                .build();
        when(userRepository.findByEmail("test@go.pl")).thenReturn(Optional.of(user));
        when(userRepository.save(any(User.class))).then(AdditionalAnswers.returnsFirstArg());

        ProfileDto profileDtoEdited = profileService.editUserProfile(email, profileDtoNew);

        Assert.assertEquals(profileDtoEdited.getFbId(), profileDtoNew.getFbId());
        Assert.assertEquals(profileDtoEdited.getLinkedinId(), profileDtoNew.getLinkedinId());
    }


}
