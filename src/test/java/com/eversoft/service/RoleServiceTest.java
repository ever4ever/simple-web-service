package com.eversoft.service;

import com.eversoft.dto.RoleModel;
import com.eversoft.model.*;
import com.eversoft.repository.RoleRepository;
import com.eversoft.service.role.RoleServceImp;
import com.eversoft.service.role.RoleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class RoleServiceTest {

    @Mock
    private RoleRepository roleRepository;
    private RoleService roleService;

    @Before
    public void setUp() {
        ModelMapper modelMapper = new ModelMapper();
        roleService = new RoleServceImp(roleRepository, modelMapper);
    }

    @Test
    public void saveRoleTest() {
        ModelMapper modelMapper = new ModelMapper();
        User userT = User.builder()
                .surname("test")
                .email("test@email.com")
                .password("password")
                .build();
        RoleType roleTypeT = RoleType.builder()
                .id("id2")
                .name(RoleTypeEnum.USER)
                .build();

        RoleModel roleModel = new RoleModel(userT, roleTypeT);
        Role roleTorep = modelMapper.map(roleModel, Role.class);

        when(roleRepository.save(any(Role.class))).thenReturn(roleTorep);

        RoleModel responseSimpleModelResponse = this.roleService.saveRole(roleModel);

        Assert.assertEquals(responseSimpleModelResponse.getRoleType().getName(), roleTypeT.getName());
        Assert.assertEquals(responseSimpleModelResponse.getUser().getEmail(), userT.getEmail());
    }
}
