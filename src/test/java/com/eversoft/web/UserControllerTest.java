package com.eversoft.web;

import com.eversoft.SimpleApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleApplication.class)
@TestPropertySource(locations = "classpath:application-tests.yaml")
@ActiveProfiles(value = "tests")
@AutoConfigureMockMvc(addFilters = false)
public class UserControllerTest {
    @Autowired
    private WebApplicationContext context;

    protected MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    public void givenIncorrectEmail_whenFindUserByEmail_expectNotFoundException() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/users?email=1")
        ).andExpect(status().isNotFound());
    }

    @Test
    public void giveCorrectEmail_whenFindUserByEmail_expectGoodResponseAndStatusOk() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/api/users/email").param("email", "sylwia")
        ).andExpect(status().isOk());

        Assert.assertTrue(resultActions.andReturn().getResponse().getContentAsString().contains("sylwia@go.pl"));
    }

    @Test
    public void sendUserId_whenGetUserById_expectUser() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/api/users").param("userId", "iddwa")
        ).andExpect(status().isOk());

        Assert.assertTrue(resultActions.andReturn().getResponse().getContentAsString().contains("piotr"));
    }

    @Test
    public void sendUserId_whenDeleteuser_expectMessageDeleted() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                post("/api/users/delete").param("userId", "iddwa")
        ).andExpect(status().isOk());

        Assert.assertTrue(resultActions.andReturn().getResponse().getContentAsString().contains("Deleted"));
    }


}
