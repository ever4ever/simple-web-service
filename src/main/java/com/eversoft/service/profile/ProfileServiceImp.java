package com.eversoft.service.profile;

import com.eversoft.dto.ProfileDto;
import com.eversoft.dto.UserDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.Profile;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfileServiceImp implements ProfileService {

    private final ProfileRepository profileRepository;
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    @Override
    public ProfileDto save(ProfileDto profile) {
        return modelMapper.map(this.profileRepository.save(modelMapper.map(profile, Profile.class)), ProfileDto.class);
    }

    @Override
    public ProfileDto editUserProfile(String email, ProfileDto profileDto) {

        User user = userRepository.findByEmail(email).orElseThrow(NotFoundException::new);
        user.getProfile().setFbId(profileDto.getFbId());
        user.getProfile().setLinkedinId(profileDto.getLinkedinId());

        userRepository.save(user);
        return modelMapper.map(
                userRepository.findByEmail(email).orElseThrow(NotFoundException::new).getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto getUserProfileByUserId(String userId) {
        return modelMapper.map(userRepository.findByEmail(userId).orElseThrow(NotFoundException::new).getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto getLoggedUserProfile() {
        User user = userRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(NotFoundException::new);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto editUserProfileByUserId(String userId, ProfileDto profileDto) {

        User tempUser = userRepository.findByEmail(userId).orElseThrow(NotFoundException::new);
        tempUser.getProfile().setLinkedinId(profileDto.getLinkedinId());
        tempUser.getProfile().setFbId(profileDto.getFbId());
        this.userRepository.save(tempUser);
        return modelMapper.map(tempUser.getProfile(), ProfileDto.class);
    }

    @Override
    public UserDto deleteUserProfileByUserId(String userId) {

        User tempUser = userRepository.findByEmail(userId).orElseThrow(NotFoundException::new);
        tempUser.getProfile().setDeleted(true);
        ProfileDto profileN = modelMapper.map(tempUser.getProfile(), ProfileDto.class);
        modelMapper.map(this.profileRepository.save(modelMapper.map(profileN, Profile.class)), ProfileDto.class);
        Profile profile = new Profile();
        tempUser.setProfile(profile);
        return modelMapper.map(this.userRepository.save(tempUser), UserDto.class);
    }


}
