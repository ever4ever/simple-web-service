package com.eversoft.service.profile;

import com.eversoft.dto.ProfileDto;
import com.eversoft.dto.UserDto;

public interface ProfileService {
    ProfileDto save(ProfileDto profile);

    ProfileDto editUserProfile(String email, ProfileDto profileDto);

    ProfileDto getLoggedUserProfile();

    ProfileDto getUserProfileByUserId(String userId);

    ProfileDto editUserProfileByUserId(String userId, ProfileDto profileDto);

    UserDto deleteUserProfileByUserId(String userId);

}
