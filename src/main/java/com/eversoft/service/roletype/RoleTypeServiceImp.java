package com.eversoft.service.roletype;

import com.eversoft.exception.NotFoundException;
import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;
import com.eversoft.repository.RoleTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleTypeServiceImp implements RoleTypeService {

    private final RoleTypeRepository roleTypeRepository;

    @Override
    public RoleType getRoleByName(RoleTypeEnum name) {
        return roleTypeRepository.findByName(name).orElseThrow(NotFoundException::new);
    }
}
