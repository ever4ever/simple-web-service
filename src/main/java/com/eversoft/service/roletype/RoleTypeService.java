package com.eversoft.service.roletype;

import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;

public interface RoleTypeService {
    RoleType getRoleByName(RoleTypeEnum name);
}

