package com.eversoft.service.user;

import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterRespone;
import com.eversoft.dto.UserRegisterDto;
import com.eversoft.dto.UsersListFilterDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserService {
    UserDto getUserByEmail(String email);

    UserDto registerUser(UserRegisterDto userDto);

    Page<UserFilterRespone> findUsers(UsersListFilterDto usersListFilterDto, Pageable pageable);

    UserDto getUserById(String userId);

    UserDto editUserByUserId(UserDto userDto);

    UserDto deleteUserByUserId(String userId);
}
