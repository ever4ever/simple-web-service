package com.eversoft.service.user;

import com.eversoft.dto.*;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.Profile;
import com.eversoft.model.QUser;
import com.eversoft.model.RoleTypeEnum;
import com.eversoft.model.User;
import com.eversoft.repository.UserRepository;
import com.eversoft.service.profile.ProfileService;
import com.eversoft.service.role.RoleService;
import com.eversoft.service.roletype.RoleTypeService;
import com.querydsl.core.BooleanBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final RoleService roleService;
    private final RoleTypeService roleTypeService;
    private final ProfileService profileService;

    @Resource(name = "tokenStore")
    TokenStore tokenStore;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDto getUserByEmail(String email) {

        return modelMapper.map(
                userRepository.findByEmail(email).orElseThrow(NotFoundException::new), UserDto.class
        );
    }

    @Override
    public UserDto registerUser(UserRegisterDto userRegisterDto) {
        User user = modelMapper.map(userRegisterDto, User.class);
        Profile profile = new Profile();
        profile.setFbId(userRegisterDto.getFbId());
        profile.setLinkedinId(userRegisterDto.getLinkedinId());
        user.setProfile(profile);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Set roleSet = new HashSet();
        roleSet.add(new RoleModel(user, roleTypeService.getRoleByName(RoleTypeEnum.USER)));
        user.setRole(roleSet);
        return modelMapper.map(this.userRepository.save(user), UserDto.class);
    }

    @Override
    public Page<UserFilterRespone> findUsers(UsersListFilterDto usersListFilterDto, Pageable pageable) {

        BooleanBuilder booleanBuilder = new BooleanBuilder();

        if (!StringUtils.isBlank(usersListFilterDto.getEmail())) {
            booleanBuilder.and(QUser.user.email.contains(usersListFilterDto.getEmail()));
        }
        if (!StringUtils.isBlank(usersListFilterDto.getSurname())) {
            booleanBuilder.and(QUser.user.surname.contains(usersListFilterDto.getSurname()));
        }
        if (!StringUtils.isBlank(usersListFilterDto.getName())) {
            booleanBuilder.and(QUser.user.name.contains(usersListFilterDto.getName()));
        }

        Page<User> page = userRepository.findAll(booleanBuilder.getValue(), pageable);

        return page.map(s -> modelMapper.map(s, UserFilterRespone.class));
    }

    @Override
    public UserDto getUserById(String userId) {
        return modelMapper.map(
                userRepository.findById(userId).orElseThrow(NotFoundException::new), UserDto.class
        );
    }


    @Override
    public UserDto editUserByUserId(UserDto userDto) {

        User tempUser = userRepository.findById(userDto.getId()).orElseThrow(NotFoundException::new);
        modelMapper.map(userDto, tempUser);
        return modelMapper.map(this.userRepository.save(tempUser), UserDto.class);
    }

    @Override
    public UserDto deleteUserByUserId(String userId) {

        User tempUser = userRepository.findById(userId).orElseThrow(NotFoundException::new);

        tempUser.setDeleted(true);
        tempUser.getProfile().setDeleted(true);
        this.userRepository.save(tempUser);

        List<String> tokenValues = new ArrayList<String>();
        Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientIdAndUserName("client", tempUser.getEmail());
        tokens.forEach(s -> tokenStore.removeAccessToken(s));
        return modelMapper.map(tempUser, UserDto.class);
    }
}
