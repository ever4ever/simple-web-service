package com.eversoft.service.role;

import com.eversoft.dto.RoleModel;
import com.eversoft.model.Role;
import com.eversoft.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleServceImp implements RoleService {

    private final RoleRepository roleRepository;
    private final ModelMapper modelMapper;

    @Override
    public RoleModel saveRole(RoleModel roleModel) {
        return modelMapper.map(roleRepository.save(modelMapper.map(roleModel, Role.class)), RoleModel.class);
    }
}
