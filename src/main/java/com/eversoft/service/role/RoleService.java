package com.eversoft.service.role;

import com.eversoft.dto.RoleModel;

public interface RoleService {
    RoleModel saveRole(RoleModel roleModel);
}
