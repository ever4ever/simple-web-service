package com.eversoft.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactoryPostgres {

    private static BasicDataSource dataSource;

    public static Connection getConnection() throws SQLException {

        if (dataSource == null) {
            dataSource = new BasicDataSource();
            dataSource.setUrl("jdbc:postgresql://52.29.160.254:5432/logs_db");
            dataSource.setDriverClassName("org.postgresql.Driver");
            dataSource.setDriver(new Driver());
            dataSource.setUsername("postgres");
            dataSource.setPassword("root");
        }
        return dataSource.getConnection();
    }
}

