package com.eversoft.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.AuditorAware;

@Configuration
@Profile("tests")
public class JpaAuditingConfigurationTests {

    @Bean
    public AuditorAware<String> auditorProvider() {
        return null;
    }
}
