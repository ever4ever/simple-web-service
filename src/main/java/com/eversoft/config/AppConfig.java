package com.eversoft.config;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.Condition;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public ModelMapper modelMapper() {

        Condition condition = new Condition() {
            @Override
            public boolean applies(MappingContext mappingContext) {
                return !StringUtils.isBlank(mappingContext.getSource().toString());
            }
        };

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.getConfiguration().setPropertyCondition(condition);
        return modelMapper;
    }

}
