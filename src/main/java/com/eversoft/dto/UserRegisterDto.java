package com.eversoft.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserRegisterDto {

    @NotBlank
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")
    private String password;
    @NotBlank
    private String passwordReEnter;
    @NotBlank
    private String surname;
    private String name;
    @NotBlank
    @Email
    private String email;
    private String fbId;
    private String linkedinId;
}
