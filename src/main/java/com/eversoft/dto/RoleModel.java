package com.eversoft.dto;

import com.eversoft.model.RoleType;
import com.eversoft.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleModel {

    @NotNull
    private User user;
    @NotNull
    private RoleType roleType;

}
