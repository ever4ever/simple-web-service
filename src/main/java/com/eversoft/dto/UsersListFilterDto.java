package com.eversoft.dto;

import lombok.Data;

@Data
public class UsersListFilterDto {

    private String email;
    private String name;
    private String surname;
}
