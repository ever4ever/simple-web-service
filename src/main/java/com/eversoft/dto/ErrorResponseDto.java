package com.eversoft.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ErrorResponseDto {
    private List<Error> errors;

    public ErrorResponseDto(int code) {
        this.errors = Arrays.asList(new Error(code));
    }

    public ErrorResponseDto(List<Integer> codes) {
        this.errors = codes.stream().map(code -> new Error(code)).collect(Collectors.toList());
    }

    @Data
    @AllArgsConstructor
    public static class Error {
        private int errorCode;
    }
}
