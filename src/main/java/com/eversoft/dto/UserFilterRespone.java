package com.eversoft.dto;

import lombok.Data;

@Data
public class UserFilterRespone {

    private String id;
    private String email;
    private String name;
    private String surname;

}
