package com.eversoft.dto;

import lombok.Data;

@Data
public class ProfileDto {

    private String id;
    private String fbId;
    private String linkedinId;
}
