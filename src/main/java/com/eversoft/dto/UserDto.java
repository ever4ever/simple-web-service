package com.eversoft.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserDto {

    private String id;
    @NotBlank
    private String email;
    private String name;
    private String surname;
    private Boolean deleted;
}
