package com.eversoft.web;

import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterRespone;
import com.eversoft.dto.UserRegisterDto;
import com.eversoft.dto.UsersListFilterDto;
import com.eversoft.exception.BadRequestException;
import com.eversoft.exception.PassworsDoesNotMatch;
import com.eversoft.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/users")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {
    private final UserService userService;

    @GetMapping("/email")
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<UserDto> findUser(@RequestParam String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> registerUser(@RequestBody @Valid UserRegisterDto userRegisterDto) {
        return ResponseEntity.ok(userService.registerUser(userRegisterDto));
    }


    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<Page<UserFilterRespone>> findAllUsers(UsersListFilterDto usersListFilterDto, Pageable pageable) {
        return ResponseEntity.ok(userService.findUsers(usersListFilterDto, pageable));
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAnyAuthority('EDIT_USER_ADMIN')")
    public ResponseEntity<UserDto> editUserByuserId(@RequestBody @Valid UserDto userDto) {
        return ResponseEntity.ok(userService.editUserByUserId(userDto));
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAnyAuthority('EDIT_USER_ADMIN')")
    public ResponseEntity<String> deleteUserByUserId(@RequestParam String userId) {
        userService.deleteUserByUserId(userId);
        return ResponseEntity.ok().body("Deleted");
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<UserDto> getUserById(@RequestParam String userId) {
        return ResponseEntity.ok(userService.getUserById(userId));
    }

    @ExceptionHandler(BadRequestException.class)
    public void handleException() {
        throw new PassworsDoesNotMatch();
    }
}
