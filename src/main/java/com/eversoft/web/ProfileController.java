package com.eversoft.web;


import com.eversoft.dto.ProfileDto;
import com.eversoft.dto.UserDto;
import com.eversoft.service.profile.ProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/users/profile")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping("/my")
    @PreAuthorize("hasAnyAuthority('USER','EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<ProfileDto> getLoggedUserProfile() {
        return ResponseEntity.ok(profileService.getLoggedUserProfile());
    }

    @GetMapping("/email")
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN')")
    public ResponseEntity<ProfileDto> registerUserProfile(@RequestParam String email, @RequestBody @Valid ProfileDto profileDto) {
        return ResponseEntity.ok(profileService.editUserProfile(email, profileDto));
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<ProfileDto> getUserProfileByUserId(@RequestParam String userId) {
        return ResponseEntity.ok(profileService.getUserProfileByUserId(userId));
    }

    @PostMapping("/edit")
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<ProfileDto> editUserProfileByUserId(@RequestParam String userId, @RequestBody @Valid ProfileDto profileDto) {
        return ResponseEntity.ok(profileService.editUserProfileByUserId(userId, profileDto));
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAnyAuthority('EDIT_PROFILE_ADMIN', 'EDIT_USER_ADMIN')")
    public ResponseEntity<UserDto> deleteUserProfileByuserId(@RequestParam String userId) {
        return ResponseEntity.ok(profileService.deleteUserProfileByUserId(userId));
    }


}
