package com.eversoft.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@FlywayDataSource
public class Role extends Auditable {

    @ManyToOne
    @JoinColumn(name = "user_table", nullable = false)
    private User user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "role_type", nullable = false)
    private RoleType roleType;

}
