package com.eversoft.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@FlywayDataSource
public class Profile extends Auditable {

    @Column(name = "fb_id")
    private String fbId;

    @Column(name = "linkedin_id")
    private String linkedinId;

}
