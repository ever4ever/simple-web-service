package com.eversoft.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@FlywayDataSource
@Table(name = "user_table", schema = "public")
public class User extends Auditable {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String password;
    private String name;
    private String surname;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Role> role;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "profile")
    private Profile profile;

}
