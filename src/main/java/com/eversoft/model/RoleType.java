package com.eversoft.model;

import javax.persistence.*;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@FlywayDataSource
@Table(name = "Role_type")
public class RoleType extends Auditable {

    @Enumerated(EnumType.STRING)
    private RoleTypeEnum name;

}
