package com.eversoft.repository;

import com.eversoft.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends QuerydslPredicateExecutor<User>, JpaRepository<User, String> {
    Optional<User> findByEmail(String email);
}
