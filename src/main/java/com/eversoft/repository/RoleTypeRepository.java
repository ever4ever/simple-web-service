package com.eversoft.repository;

import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleTypeRepository extends JpaRepository<RoleType, String> {
    Optional<RoleType> findByName(RoleTypeEnum name);
}
