package com.eversoft.repository;

import com.eversoft.model.Role;
import com.eversoft.model.RoleType;
import com.eversoft.model.RoleTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
}
