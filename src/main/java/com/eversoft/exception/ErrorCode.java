package com.eversoft.exception;

public enum ErrorCode {
    NOT_FOUND(404),
    BAD_REQUEST(400),
    PASSWORDS_DOES_NOT_MATCH(40001);

    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
