package com.eversoft.exception;

public class BadRequestException extends BaseException {
    public BadRequestException() {
        super(ErrorCode.BAD_REQUEST);
    }
}
