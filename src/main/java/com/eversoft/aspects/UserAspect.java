package com.eversoft.aspects;


import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.MDC;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Log4j2
@Component
@EnableAspectJAutoProxy
@Profile("!tests")
public class UserAspect {

    final Level SENDLOGS = Level.forName("SENDLOGS", 10);

    @After("execution(public * com.eversoft.service.user.UserServiceImpl.*(*))")
    public void saveMethodExecution(JoinPoint joinPoint) {
        MDC.put("userId", SecurityContextHolder.getContext().getAuthentication().getName());
        MDC.put("method", joinPoint.getSignature().toString());
        String params = Arrays.asList(joinPoint.getArgs()).stream()
                .map(Object::toString)
                .collect(Collectors.joining("-"));
        MDC.put("params", params);
        log.log(SENDLOGS, Arrays.asList(joinPoint.getArgs()));
    }
}
