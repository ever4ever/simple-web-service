create table profile
(
    id                 varchar(255) not null,
    created_by         varchar(255),
    created_date       timestamp,
    deleted            boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    fb_id              varchar(255),
    linkedin_id        varchar(255),
    primary key (id)
);
create table role
(
    id                 varchar(255) not null,
    created_by         varchar(255),
    created_date       timestamp,
    deleted            boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    role_type          varchar(255) not null,
    user_table         varchar(255) not null,
    primary key (id)
);
create table role_type
(
    id                 varchar(255) not null,
    created_by         varchar(255),
    created_date       timestamp,
    deleted            boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    name               varchar(255),
    primary key (id)
);
create table user_table
(
    id                 varchar(255) not null,
    created_by         varchar(255),
    created_date       timestamp,
    deleted            boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    email              varchar(255) not null,
    name               varchar(255),
    password           varchar(255) not null,
    surname            varchar(255),
    profile            varchar(255),
    primary key (id)
);
alter table role
    add constraint FK82qp6ib1n9cr66ia9a3q3b5rm foreign key (role_type) references role_type;
alter table role
    add constraint FKoo4bjof6x9tbtuov58hm6vhij foreign key (user_table) references user_table;
alter table user_table
    add constraint FKpvb09hh6owjl553leyju63fue foreign key (profile) references profile;
