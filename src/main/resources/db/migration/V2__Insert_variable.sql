INSERT INTO ROLE_TYPE
VALUES ('idjeden', 'auto-generate', '2020-01-01', false, null, null, 1, 'USER');
INSERT INTO ROLE_TYPE
VALUES ('iddwa', 'auto-generate', '2020-01-01', false, null, null, 1, 'EDIT_PROFILE_ADMIN');
INSERT INTO ROLE_TYPE
VALUES ('idtrzy', 'auto-generate', '2020-01-01', false, null, null, 1, 'EDIT_USER_ADMIN');

INSERT INTO PROFILE
VALUES ('idjeden', 'auto-generate', '2020-01-01', false, null, null, 1, 'fb_id_1', 'linkedin_id_1');
INSERT INTO PROFILE
VALUES ('iddwa', 'auto-generate', '2020-01-01', false, null, null, 1, 'fb_id_2', 'linkedin_id_2');
INSERT INTO PROFILE
VALUES ('idtrzy', 'auto-generate', '2020-01-01', false, null, null, 1, 'fb_id_3', 'linkedin_id_3');

INSERT INTO user_table
VALUES ('idjeden', 'auto-generate', '2020-01-01', false, null, null, 1, 'sylwia', 'sylwia@go.pl',
        '$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam', 'kowalska', 'idjeden');
INSERT INTO user_table
VALUES ('iddwa', 'auto-generate', '2020-01-01', false, null, null, 1, 'piotr@go.pl', 'popl',
        '$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam', 'piotr', 'iddwa');
INSERT INTO user_table
VALUES ('idtrzy', 'auto-generate', '2020-01-01', false, null, null, 1, 'marek@go.pl', 'marek',
        '$2a$10$dZYbGAl2RJ1AjGJvbckpp./zSB.AnnrKNSLZ7GA2bBQ0cwlzF/Dam', 'lew', 'idtrzy');

INSERT INTO ROLE
VALUES ('idjeden', 'auto-generate', '2020-01-01', false, null, null, 1, 'idjeden', 'idjeden');
INSERT INTO ROLE VALUES ('iddwa','auto-generate', '2020-01-01',false, null, null,1, 'iddwa', 'idjeden');
INSERT INTO ROLE VALUES ('idtrzy','auto-generate', '2020-01-01',false, null, null,1, 'iddwa', 'iddwa');
INSERT INTO ROLE VALUES ('idcztery','auto-generate', '2020-01-01',false, null, null,1, 'idtrzy', 'idjeden');
INSERT INTO ROLE VALUES ('idpiec','auto-generate', '2020-01-01',false, null, null,1, 'idtrzy', 'iddwa');
INSERT INTO ROLE VALUES ('idszesc','auto-generate', '2020-01-01',false, null, null,1, 'idtrzy', 'idtrzy');
