## Oauth ENDPOINTS

http://localhost:8080/oauth/token?grant_type=password&username=sylwia@go.pl&password=root

POST

Params:
1. grant_type = password
2. username from db
3. password from db

Authorisation: 
**client**
**root*

## ENDPOINTS
1. REGISTRATION POST
http://localhost:8080/api/users/register

{
  "email": "tnowy@go.pl",
  "password": "Kaczka12&!",
  "passwordReEnter": "Kaczka12&!",
  "surname": "test2",
  "name": "test3",
  "fbId": "fbId",
  "linkedinId": "linkedinId"
}

** ALL ABOVE NEEDS VALIDTOKEN**

2. FIND BY EMAIL GET
http://localhost:8080/api/users?email=sylwia@go.pl



3. EDIT MY PROFILE GET
http://localhost:8080/api/users/profile/mine

{
  "fbId": "newFB",
  "linkedinId": "newLIN"
}

4. EDIT USER PROFILE GET
http://localhost:8080/api/users/profile/{email}

{
  "fbId": "newFB",
  "linkedinId": "newLIN"
}

5. GET ALL USERS GET
http://localhost:8080/api/users/get-all-users
{
  "email": "string",
  "name": "string",
  "surname": "string",
  "page": 0,
  "size": 0,
  "sortColumn": "string"
}

5. POST edit-user-by-user-id
http://localhost:8080/api/users/edit-user-by-user-id?userId=XXXXXX
{
  "id": "",
  "email": "",
  "name": "",
  "surname": ""
}
Empty or null is filtered out on mapping

## FLYWAY

FlyWay:
1. **VX__name.sql**
2. mvn flyway:baseline - integrate flyway with existing database
3. mvn flyway:migrate - run migration


## Swagger

Swagger link:
1. **http://localhost:8080/swagger-ui.html**

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
